

/*

 *

 *	version.h

 *  

 * 

 *  @author Steve Hoffmann, steve@bioinf.uni-leipzig.de

 *  @company Bioinformatics, University of Leipzig 

 *  @date 07.08.2017 19:32:04 CEST  

 *

 */





#define VERSION "0.3.4"

#define REVISION "ge5dee47"

#define TIME "2018-12-25 20:39:07 +0100"



