CC?=gcc
LD=${CC}
CFLAGS +=  -Wall -pedantic -std=c99 -g -O3 -DSORTEDUNMAPPED -D_LARGEFILE_SOURCE -D_FILE_OFFSET_BITS=64 -DDBGNFO -DSHOWALIGN -DDBGLEVEL=0 -DPROGNFO -Ilibs -Ilibs/sufarray -Isamtools
CFLAGS += `pkg-config --cflags htslib`
INC := -I include
CTAGS = ctags > tags
LIB += -lm -lpthread -lz -lncurses -L libs -lform -lmenu -L/usr/local/lib/
LIB += `pkg-config --libs htslib`
LIB += "-Wl,-rpath,`pkg-config --variable=libdir htslib`"


PRGTARGETS := segemehl haarz

LIBDIR := libs
BUILDDIR:= build
TARGETDIR := .
TARGETEXT := .x
SRCEXT := c


SOURCES := $(shell find $(LIBDIR) -type f -name *.$(SRCEXT))
SOURCES := $(filter-out $(EXCLUDE), $(SOURCES)) 

PRGSOURCES := $(patsubst %,$(LIBDIR)/%.c,$(PRGTARGETS))
LIBSOURCES := $(filter-out $(PRGSOURCES), $(SOURCES)) 

OBJECTS := $(patsubst $(LIBDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
LIBOBJECTS :=  $(patsubst $(LIBDIR)/%,$(BUILDDIR)/%,$(LIBSOURCES:.$(SRCEXT)=.o))


$(PRGTARGETS): $(OBJECTS)
	@echo "Linking $@";	
	$(LD) $(LIBOBJECTS) $(BUILDDIR)/$@.o -o $(TARGETDIR)/$@$(TARGETEXT) $(LIB) $(LDFLAGS)


$(BUILDDIR)/%.o: $(LIBDIR)/%.c
	@echo "Building... library for source $@";
	@mkdir -p $(BUILDDIR)	
	$(CC) $(CFLAGS) $(INC) -c -o $@ $<

all: $(PRGTARGETS)

clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(BUILDDIR) $(PRGTARGETS)"; $(RM) -r $(BUILDDIR) $(PRGTARGETS)


.PHONY: clean

